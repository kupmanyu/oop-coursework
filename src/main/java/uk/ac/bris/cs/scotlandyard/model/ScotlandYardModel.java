package uk.ac.bris.cs.scotlandyard.model;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;
import static uk.ac.bris.cs.scotlandyard.model.Colour.BLACK;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.function.Consumer;
import uk.ac.bris.cs.gamekit.graph.Edge;
import uk.ac.bris.cs.gamekit.graph.Graph;
import uk.ac.bris.cs.gamekit.graph.ImmutableGraph;

// TODO implement all methods and pass all tests
public class ScotlandYardModel implements ScotlandYardGame, Consumer<Move>{
    private List<Boolean> rounds;
    private Graph<Integer, Transport> graphs;
    private final ArrayList<ScotlandYardPlayer> players;
    private final ArrayList<Colour> colours;
    private final ImmutableGraph<Integer, Transport> igraph;
    private int current = 0;
    private int round = NOT_STARTED;
    private Set<Colour> winner = new HashSet<>();
    private int mrXlocation;
    private Set<Move> moves;
    private Player cplayer;
    private TicketMove tmove;
    private PassMove pmove;
    private DoubleMove dmove;
    private TicketMove firstmove;
    private TicketMove secondmove;
    private TicketMove secretmove;
    private ArrayList<Edge<Integer, Transport>> possibleEdges;
    private ArrayList<Integer> plocations;
    private ArrayList<Spectator> spectators = new ArrayList<>();
//    private Colour winningPlayer;

    public ScotlandYardModel(List<Boolean> rounds, Graph<Integer, Transport> graph,
                    PlayerConfiguration mrX, PlayerConfiguration firstDetective,
                    PlayerConfiguration... restOfTheDetectives) {
        roundsCheck(rounds);
        graphCheck(graph);
        mrXCheck(mrX);
        ArrayList<PlayerConfiguration> configurations = new ArrayList<>();
        for (PlayerConfiguration configuration : restOfTheDetectives)
            configurations.add(requireNonNull(configuration));
        configurations.add(0, requireNonNull(firstDetective));
        configurations.add(0, requireNonNull(mrX));
        playerCheck(configurations, mrX);
        players = toSYPlayer(configurations);
        colours = toPlayer(players);
        igraph = new ImmutableGraph<>(graph);            
    }

    private void roundsCheck(List<Boolean> rounds) {
            this.rounds = requireNonNull(rounds);
            if (rounds.isEmpty()) {
                throw new IllegalArgumentException("Empty rounds");
            }
        }

    private void graphCheck(Graph<Integer, Transport> graph) {
            this.graphs = requireNonNull(graph);
            if (graph.isEmpty()) {
                throw new IllegalArgumentException("Empty graph");
            }
        }

    private void mrXCheck(PlayerConfiguration mrX) {
            if (mrX.colour != BLACK) {
                throw new IllegalArgumentException("Mr. X should be Black");
            }
        }

    private void playerCheck(ArrayList<PlayerConfiguration> configurations, PlayerConfiguration mrX) {
            Set<Integer> set = new HashSet<>();
            Set<Colour> set_colour = new HashSet<>();
            for (PlayerConfiguration configuration : configurations) {
                if (set.contains(configuration.location))
                    throw new IllegalArgumentException("Duplicate location");
                if (set_colour.contains(configuration.colour))
                    throw new IllegalArgumentException("Duplicate colour");
                set.add(configuration.location);
                set_colour.add(configuration.colour);
                if (configuration.tickets.values().size() != 5)
                    throw new IllegalArgumentException("Missing Tickets");
                if (configuration != mrX)
                    if (configuration.tickets.get(SECRET) != 0)
                        throw new IllegalArgumentException("Detectives cannot have Secret Ticket");
                    else if (configuration.tickets.get(DOUBLE) != 0)
                        throw new IllegalArgumentException("Detectives cannot have Double Ticket");
            }
        }
    
    private ArrayList<ScotlandYardPlayer> toSYPlayer(ArrayList<PlayerConfiguration> configurations) {
            ArrayList<ScotlandYardPlayer> syplayers = new ArrayList<>();
            for (PlayerConfiguration configuration : configurations)
                syplayers.add(new ScotlandYardPlayer(configuration.player, configuration.colour, configuration.location, configuration.tickets));
            return syplayers;
        }
    
    private ArrayList<Colour> toPlayer(ArrayList<ScotlandYardPlayer> players) {
            ArrayList<Colour> colours = new ArrayList<>();
            for (ScotlandYardPlayer player : players)
                colours.add(player.colour());
            return colours;
    }

    @Override
    public void registerSpectator(Spectator spectator) {
            requireNonNull(spectator);
            if (spectators.contains(spectator))
                throw new IllegalArgumentException();
            else
                spectators.add(spectator);
        }

    @Override
    public void unregisterSpectator(Spectator spectator) {
        requireNonNull(spectator);
        if (!spectators.contains(spectator))
            throw new IllegalArgumentException();
        else
            spectators.remove(spectator);
	}

    @Override
    public void startRotate() {
        if (isGameOver() && round == NOT_STARTED)
            throw new IllegalStateException();
        else
            requestMove(getPlayer(getCurrentPlayer()), getCurrentPlayer());
	}

    private Player getPlayer(Colour colour) {
        for (ScotlandYardPlayer player : players)
            if (player.colour() == colour)
                cplayer= player.player();
        return cplayer;
    }
    
    private void requestMove(Player player, Colour colour) {
        moves = validMoves(colour);
        player.makeMove(this, getTruePlayerLocation(colour), moves, this);
    }
    
    private void updatePlayerData(Update visitor) {
        for (ScotlandYardPlayer player : players) {
            if (player.colour() == visitor.player) {
                if (visitor.used != DOUBLE) {
                    player.location(visitor.location);
                    player.removeTicket(visitor.used);
                }
                else {
                    player.location(visitor.firstlocation);
                    player.removeTicket(visitor.used);
                }
                if (visitor.player != BLACK)
                    players.get(0).addTicket(visitor.used);
            }
        }
    }
    
    private void updateRound() {
        if (rounds.size() - round != 0)
            round++;
    }

    private void notify(Update visitor, Move move) {
        if (move.colour() != BLACK) {
            if (isGameOver()) {
                setWinner();
                spectators.forEach(spectator -> spectator.onMoveMade(this, move));
                spectators.forEach(spectator -> spectator.onGameOver(this, winner));
            }
            else {
                spectators.forEach(spectator -> spectator.onMoveMade(this, move));
            }
        }
        else {
            if (visitor.used != DOUBLE) {
                TicketMove normal;
                if (rounds.get(round))
                    normal = new TicketMove(visitor.player, visitor.used, visitor.location);
                else
                    normal = new TicketMove(visitor.player, visitor.used, mrXlocation);
                updateRound();
                spectators.forEach(spectator -> spectator.onRoundStarted(this, round));
                spectators.forEach(spectator -> spectator.onMoveMade(this, normal));
            }
            else {
                TicketMove first;
                TicketMove second;
                if (rounds.get(round))
                    first = new TicketMove(visitor.player, visitor.firstmove, visitor.firstlocation);
                else
                    first = new TicketMove(visitor.player, visitor.firstmove, getPlayerLocation(BLACK).get());
                if (rounds.get(round + 1))
                    second = new TicketMove(visitor.player, visitor.secondmove, visitor.location);
                else
                    second = new TicketMove(visitor.player, visitor.secondmove, first.destination());
                DoubleMove doubleMove = new DoubleMove(visitor.player, first, second);
                spectators.forEach(spectator -> spectator.onMoveMade(this, doubleMove));
                updateRound();
                players.get(0).removeTicket(visitor.firstmove);
                spectators.forEach(spectator -> spectator.onRoundStarted(this, round));
                spectators.forEach(spectator -> spectator.onMoveMade(this, first));
                updateRound();
                players.get(0).removeTicket(visitor.secondmove);
                players.get(0).location(visitor.location);
                spectators.forEach(spectator -> spectator.onRoundStarted(this, round));
                spectators.forEach(spectator -> spectator.onMoveMade(this, second));
            }
        }
    }

    @Override
    public void accept(Move move) {
        requireNonNull(move);
        if (!moves.contains(move))
            throw new IllegalArgumentException(move.toString());
        Update visitor = new Update();
        move.visit(visitor);
        if (visitor.location != -1)
            updatePlayerData(visitor);
        current = (current + 1) % colours.size();
        notify(visitor, move);
        if (!isGameOver()) {
            if (current != 0)
                requestMove(getPlayer(getCurrentPlayer()), getCurrentPlayer());
            else
                spectators.forEach(spectator -> spectator.onRotationComplete(this));
        }
    }
        
    private Integer getTruePlayerLocation(Colour colour) {
        for (ScotlandYardPlayer player : players)
            if (player.colour() == colour)
                return player.location();
        return -1;
    }
    
    private Set<Move> validMoves(Colour colour) {
        Set<Move> moves = new HashSet<>();
        possibleEdges = new ArrayList<>();
        possibleEdges.addAll(igraph.getEdgesFrom(igraph.getNode(getTruePlayerLocation(colour))));
        plocations = new ArrayList<>();
        for (Colour player : colours)
            if (player != colour && player != BLACK)
                plocations.add(getTruePlayerLocation(player));
        if (colour != BLACK) {
            for (Edge<Integer, Transport> possibleEdge : possibleEdges) {                     
                tmove = new TicketMove(colour, Ticket.fromTransport(possibleEdge.data()), possibleEdge.destination().value());                
                if (getPlayerTickets(colour, Ticket.fromTransport(possibleEdge.data())).get() != 0 && !plocations.contains(possibleEdge.destination().value()))
                    moves.add(tmove);
            }
            if (moves.isEmpty()) {
                pmove = new PassMove(colour);
                moves.add(pmove);
            }                
        }
        else {
            for (Edge<Integer, Transport> possibleEdge : possibleEdges) {                
                tmove = new TicketMove(colour, Ticket.fromTransport(possibleEdge.data()), possibleEdge.destination().value());
                secretmove = new TicketMove(colour, SECRET, possibleEdge.destination().value());
                if (getPlayerTickets(colour, Ticket.fromTransport(possibleEdge.data())).get() != 0 && !plocations.contains(possibleEdge.destination().value())) 
                    moves.add(tmove);
                if (getPlayerTickets(colour, SECRET).get() != 0 && !plocations.contains(possibleEdge.destination().value()))
                    moves.add(secretmove);
                ArrayList<Edge<Integer, Transport>> possibleSecEdges = new ArrayList<>();
                possibleSecEdges.addAll(igraph.getEdgesFrom(possibleEdge.destination()));
                if (rounds.size() - round >= 2) {
                    for (Edge<Integer, Transport> possibleSecEdge : possibleSecEdges) {
                        firstmove = new TicketMove (colour, Ticket.fromTransport(possibleEdge.data()), possibleEdge.destination().value());
                        secondmove = new TicketMove (colour, Ticket.fromTransport(possibleSecEdge.data()), possibleSecEdge.destination().value());
                        dmove = new DoubleMove(colour, firstmove, secondmove);
                        if (getPlayerTickets(colour, DOUBLE).get() != 0 && getPlayerTickets(colour, firstmove.ticket()).get() != 0 && !plocations.contains(firstmove.destination()) && getPlayerTickets(colour, secondmove.ticket()).get() != 0 && !plocations.contains(secondmove.destination())) {
                            moves.add(dmove);
                            if (dmove.hasSameTicket() && getPlayerTickets(colour, firstmove.ticket()).get() < 2)
                                moves.remove(dmove);
                            if (getPlayerTickets(colour, SECRET).get() >= 2) {
                                dmove = new DoubleMove(colour, SECRET, firstmove.destination(), secondmove.ticket(), secondmove.destination());
                                moves.add(dmove);
                                dmove = new DoubleMove(colour, firstmove.ticket(), firstmove.destination(), SECRET, secondmove.destination());
                                moves.add(dmove);
                                dmove = new DoubleMove(colour, SECRET, firstmove.destination(), SECRET, secondmove.destination());
                                moves.add(dmove);
                            }
                            if (getPlayerTickets(colour, SECRET).get() == 1) {
                                dmove = new DoubleMove(colour, SECRET, firstmove.destination(), secondmove.ticket(), secondmove.destination());
                                moves.add(dmove);
                                dmove = new DoubleMove(colour, firstmove.ticket(), firstmove.destination(), SECRET, secondmove.destination());
                                moves.add(dmove);
                            }
                        }                 
                    }
                }
            }
        }
        return Collections.unmodifiableSet(moves);
    }
    
    @Override
    public Collection<Spectator> getSpectators() {
            return Collections.unmodifiableList(spectators);
	}

    @Override
    public List<Colour> getPlayers() {   
            return Collections.unmodifiableList(colours);
	}

    @Override
    public Set<Colour> getWinningPlayers() {
            return Collections.unmodifiableSet(winner);
	}

    @Override
    public Optional<Integer> getPlayerLocation(Colour colour) {
        if (colours.contains(colour) && colour != BLACK)
            for (ScotlandYardPlayer player : players)
                if (colour == player.colour())
                    return Optional.of(player.location());
        if (colour == BLACK)
            if (round == NOT_STARTED)
                return Optional.of(0);
            else if (getRounds().get(round - 1)) {
                mrXlocation = players.get(0).location();
                return Optional.of(mrXlocation);  
            }
            else 
                return Optional.of(mrXlocation);
        return Optional.empty();
    }

    @Override
    public Optional<Integer> getPlayerTickets(Colour colour, Ticket ticket) {
        if (colours.contains(colour))
            for (ScotlandYardPlayer player : players)
                if (colour == player.colour())
                    return Optional.of(player.tickets().get(ticket));
        return Optional.empty();
    }

    private boolean areDetectivesStuck() {
        int tickets = 0;
        for (ScotlandYardPlayer player : players)
            if (player.isDetective()) {
                tickets += getPlayerTickets(player.colour(), BUS).get();
                tickets += getPlayerTickets(player.colour(), TAXI).get();
                tickets += getPlayerTickets(player.colour(), UNDERGROUND).get();
            }
        return (tickets == 0);
    }

    private boolean isMrXCaught() {
        for (ScotlandYardPlayer player : players)
            if (player.isDetective())
                if (player.location() == players.get(0).location())
                    return true;
        return false;
    }

    private void setWinner() {
        if (round == rounds.size()) {
            winner.add(BLACK);
        }
        else if (validMoves(BLACK).isEmpty() && getCurrentPlayer() == BLACK) {
            winner.addAll(colours);
            winner.remove(BLACK);
        }
        else if (areDetectivesStuck()) {
            winner.add(BLACK);
        }
        else if (isMrXCaught()) {
            winner.addAll(colours);
            winner.remove(BLACK);
        }
    }

    @Override
    public boolean isGameOver() {
        if (round == rounds.size() && current == 0) {
            return true;
        }
        else if (validMoves(BLACK).isEmpty() && current == 0) {
            return true;
        }
        else if (areDetectivesStuck()) {
            return true;
        }
        else if (isMrXCaught()) {
            return true;
        }
        else
            return false;
	}

    @Override
    public Colour getCurrentPlayer() {
            return colours.get(current);
	}

    @Override
    public int getCurrentRound() {
            return round;
	}

    @Override
    public List<Boolean> getRounds() {
            return Collections.unmodifiableList(rounds);
	}

    @Override
    public Graph<Integer, Transport> getGraph() {
            return igraph;
	}
    
}
