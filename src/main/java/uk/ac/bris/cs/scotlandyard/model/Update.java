/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.bris.cs.scotlandyard.model;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.DOUBLE;

public class Update implements MoveVisitor{
    Colour player;
    int location;
    Ticket used;    
    Ticket firstmove;
    Ticket secondmove;
    int firstlocation;
    
    @Override
    public void visit(TicketMove move) {
        player = move.colour();
        location = move.destination();
        used = move.ticket();
    }
    
    @Override
    public void visit(DoubleMove move) {
        player = move.colour();
        location = move.finalDestination();
        used = DOUBLE;
        firstmove = move.firstMove().ticket();
        secondmove = move.secondMove().ticket();
        firstlocation = move.firstMove().destination();
    }
    
    @Override
    public void visit(PassMove move) {
        location = -1;
    }
}
